## TD5: Data Manipulation with dplyr

Using the [first name data
set of INSEE](https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2017_txt.zip), answer some of the following questions (you also have to use
**ggplot2**):

- First name frequency evolves along time?
- What can we say about ``Your name here'' (for each state, FR)?
- Is there some sort of geographical correlation with the data?
- Which state has a larger variety of names along time?
- _your own question_ (be creative)

We have demonstrated the use of the six verbs in [our demonstration of
dplyr](./TD5/TD5.Rmd).

1. Using the [given names data set of INSEE](https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2017_txt.zip), answer some of the following questions:

- First name frequency evolves along time?
- What can we say about ``Your name here'' (for each state, all the country)?
- Is there some sort of geographical correlation with the data?
- Which department has a larger variety of names along time?
- _your own question_ (be creative)

You need to use the _tidyverse_ for this analysis. Unzip the file _dpt2016_txt.zip_ (to get the **dpt2017.txt**). Read in R with this code. Note that you might need to install the `readr` package with the appropriate command.

2. Download Raw Data from the website
```{r}
file = "dpt2017_txt.zip"
if(!file.exists(file)){
  download.file("https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2017_txt.zip",
	destfile=file)
}
unzip(file)
```

3.  Build the Dataframe from file

```{r}
library(tidyverse)
library(ggplot2)

df <- read_tsv("dpt2017.txt", locale = locale(encoding = "ISO-8859-1"));
df %>% head(n=10)
```

4. Create your own R markdown (Rmd). Follow the PL guidelines,
explaining your data manipulation and plot.
  - Use all the six verbs: _select()_, _filter()_, _arrange()_, _mutate()_, _group_by()_, _summarize()_
  - Plot data with _ggplot()_


